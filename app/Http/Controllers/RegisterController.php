<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;

class RegisterController extends Controller
{
    public function register(){
        return view('authentication.register');
    }

    public function postRegister(Request $request){
        $user = Sentinel::registerAndActivate($request->all());
        $role = Sentinel::findRoleBySlug('member');
        $role->users()->attach($user);
        return redirect('/post');
    }

    //
}
