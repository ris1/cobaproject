<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;

class LoginController extends Controller
{
    public function login(){
        if(Sentinel::check()){
            return redirect('/posts');
        }
        else{
            return view('authentication.login');
        }
    }

    public function postLogin(Request $request){
        Sentinel::authenticate($request->all());
        if (Sentinel::check()){
            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    public function logout(){
        Sentinel::logout();
        return redirect('/login');
    }
    //
}
