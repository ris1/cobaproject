<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'slug' => 'admin',
            'name' => 'Admin',
        ]);
        DB::table('roles')->insert([
            'slug' => 'member',
            'name' => 'Member',
            ]);

        // $this->call(UsersTableSeeder::class);
    }
}
