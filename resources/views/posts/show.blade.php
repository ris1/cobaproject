@extends('layouts.app')
@include('layouts.navbar')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">    
                    <h3>{{$post->title}}</h3>
                </div>
                <div class="card-body">    
                    <p>{{$post->body}}</p>
                    <small>Created at {{$post->created_at}} by {{$post->user_name}}. 
                @if (Auth::user()->id == $post->user_id || Auth::user()->type == 'admin')    
                        <a href="/posts/{{$post->id}}/edit">Edit</a>
                    </small><br><br>
                    {!!Form::open(['action'=> ['PostsController@destroy', $post->id], ' method'=>'POST'])!!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('Delete')}}
                    {!!Form::close()!!}
                @endif
                    <br><br><a href="/posts" >Go back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection