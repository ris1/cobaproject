@extends('layouts.app')
@section('main-content')
    @include('layouts.navbar')
    <div class="container-login100" style="color:white;">
        <div class="wrap-login100" style="margin-top:5%;">
        {!! Form::open(['action' => 'PostsController@store','method' => 'POST']) !!}
        <div class="login100-form-title p-b-40">
            <h3>Create Posts</h3>
        </div>
        
        <div class="wrap-input100 validate-input">
            <input class="input100" type="text" name="title">
            <span class="focus-input100" data-placeholder="Title"></span>
        </div>

        <div class="wrap-input100 validate-input">
                <input class="input100" style=";" type="textarea" name="body">
                <span class="focus-input100" data-placeholder="Body Text"></span>
        </div>
        
        <div class="container-login100-form-btn">
            <div class="wrap-login100-form-btn">
                <div class="login100-form-bgbtn"></div>
                <button type="submit" class="login100-form-btn">
                    Post!
                </button>
            </div>
        </div>


        
        {!! Form::close() !!}
        <br><br><a href="/posts" >Go back</a>
        </div>
    </div>
@endsection