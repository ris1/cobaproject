@extends('layouts.app')

@section('main-content')
@include('layouts.navbar')
<body style="background:linear-gradient(-90deg,darkmagenta , skyblue ); overflow-y:hidden;">
    <div class="container" style="margin-top:230px; text-align:center; ">
            <!-- Example row of columns -->
            <div class="row">
                @if(count($posts)>=1)
                @foreach ($posts as $post)
                      <div class="col-md">
                        <h2>{{$post->user_id}}:<br> <a href="/posts/{{$post->id}}">{{$post->title}}</a></h2>
                        <small>on {{$post->created_at}}</small>
                        <br><br>
                    </div>
                    @endforeach
                @else
                    <p>No Post Found</p>
                @endif
            </div>
            <br><br>
            <a href="/" style="color:aliceblue;">Go back</a> <br>
            <a href="/posts/create"style="color:aliceblue;">Create Posts</a><br><br>
    
        </div>
    </div>
</body>
@endsection