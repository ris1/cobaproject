@extends('layouts.app')
@include('layouts.navbar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Edit Posts</h3>
                </div>    
                <div class="card-body">
                    {!! Form::open(['action' => ['PostsController@update',$post->id],'method' => 'POST']) !!}
                        {{Form::label('title', 'Title')}}<br>
                        {{Form::text('title',$post->title)}}<br><br>

                        {{Form::label('body', 'Body')}}<br>
                        {{Form::textarea('body',$post->body)}}<br><br>

                        {{Form::hidden('_method','PUT')}}
                        {{Form::submit('Update!')}}<br>
                    {!! Form::close() !!}
                </div>
                <br><br><a href="/posts" >Go back</a>
            </div>
        </div>
    </div>
</div>
@endsection