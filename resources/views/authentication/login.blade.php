@extends('layouts.app')

@section('title')
    Login!
@endsection

@section('main-content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100" style="opacity:0.9; background-color: rgba(250, 250, 250, 0.7);">
                <form class="login100-form validate-form" action="/login" method="POST">
                    {{ csrf_field() }}
                    <span class="login100-form-title p-b-50">
                        Login!
                    </span>

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                        <input class="input100" type="text" name="email">
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input class="input100" type="password" name="password" required>
                        <span class="focus-input100" data-placeholder="Password"></span>
                    </div>

                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button type="submit" class="login100-form-btn">
                                Login!
                            </button>
                        </div>
                    </div>

                    <div class="text-center p-t-50">
                        <span class="txt1">
                            Dont have an account?
                        </span>

                        <a class="txt2" href="/register">
                            Sign Up
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
