@extends('layouts.app')

@section('title')
    Register!
@endsection

@section('main-content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" action="/register" method="POST">
                    {{ csrf_field() }}
                    <span class="login100-form-title p-b-30" style="margin-top:-15%; margin-bottom:5%">
                        Register Here
                    </span>

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                        <input class="input100" type="text" name="email">
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>

                    <div class="wrap-input100 validate-input">
                        <input class="input100" type="text" name="first_name">
                        <span class="focus-input100" data-placeholder="First Name"></span>
                    </div>

                    <div class="wrap-input100 validate-input">
                        <input class="input100" type="text" name="last_name">
                        <span class="focus-input100" data-placeholder="Last Name"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input class="input100" type="password" name="password">
                        <span class="focus-input100" data-placeholder="Password"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input class="input100" type="password_confirmation" name="password">
                        <span class="focus-input100" data-placeholder="Re-type Your Password"></span>
                    </div>

                    <div class="container-login80-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button type="submit" class="login100-form-btn" >
                                Register!
                            </button>

                        </div>
                    </div>

                    <div class="text-center p-t-50">
                        <span class="txt1">
                            Do you have an account?
                        </span>

                        <a class="txt2" href="/login">
                            Sign In
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
