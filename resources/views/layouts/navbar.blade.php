<nav class="navbar navbar-expand-md navbar-white fixed-top"  style="opacity:0.9; background-color: rgba(250, 250, 250, 0.7);">
    <a class="navbar-brand" href="/">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
            </li>
        </ul>
        @if(Sentinel::check())
        <form action="/logout" class="form-inline my-2 my-lg-0">
            {{csrf_field()}}
            <button class="login100-bgbtn" onclick="/logout">Logout</button>
        </form>
        @else
        <form action="/login" class="form-inline my-2 my-lg-0">
            <button class="login100-bgbtn" onclick="/logout">Login</button>
        </form>
        @endif
    </div>
  </nav>
