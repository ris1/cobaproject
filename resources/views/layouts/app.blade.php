<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        @yield('title')
    </title>
    @include('inc.script-url')
</head>
<body>
    @section('main-content')@show    
    @include('inc.script')
</body>
</html>